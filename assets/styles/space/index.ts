import { css } from '@emotion/react';

import MarginStyle from './margin';
import PaddingStyle from './padding';

export default css`
  ${MarginStyle}
  ${PaddingStyle}
`;
