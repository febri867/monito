import { css } from '@emotion/react';
import theme from '@/assets/styles/theme';

const { common, primary, ashpalth } = theme.palette;

const value = {
  'color-black': common.black,
  'color-white': common.white,
  'color-primary': primary.main,
  'color-ashpalt-light': ashpalth.light,
};

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ color: ${val} }`;
  });
  return result;
};

const color = css`
  ${generatedStyle()}
`;

export default color;
