import { css } from '@emotion/react';

import ColorStyle from './color';
import SizeStyle from './size';
import WeightStyle from './weight';
import AlignmentStyle from './alignment';

export default css`
  ${ColorStyle}
  ${SizeStyle}
  ${WeightStyle}
  ${AlignmentStyle}
`;
