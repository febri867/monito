import { CustomComponentsOptions } from './type';

const customComponents: CustomComponentsOptions = {
  modal: {
    maxWidth: [600],
    bgZIndex: 1000,
    contentZIndex: 1001,
  },
  topRibbon: {
    height: [52, 70],
    padding: [
      [0, 20, 0, 20],
      [0, 60, 0, 60],
    ],
    checkWidth: [105, 140],
    zIndex: 100,
  },
  stepAuth: {
    maxWidth: 325,
    height: 50,
  },
};

export default customComponents;
