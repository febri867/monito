import {
  Palette as MuiPalette,
  PaletteOptions as MuiPaletteOptions,
  PaletteColor as MuiPaletteColor,
} from '@mui/material/styles';

interface PaletteColor extends MuiPaletteColor {
  lighter?: string;
  contrast10?: string;
  contrast40?: string;
  contrast50?: string;
  contrast60?: string;
  contrast70?: string;
  contrast80?: string;
  contrast100?: string;
}

export interface PaletteOptions extends MuiPaletteOptions {
  primary: PaletteColor;
  cactus?: PaletteColor;
  mist?: PaletteColor;
  ashpalth?: PaletteColor;
  yellow?: PaletteColor;
  red?: PaletteColor;
}

export interface Palette extends MuiPalette {
  primary: PaletteColor;
  cactus: PaletteColor;
  mist: PaletteColor;
  ashpalth: PaletteColor;
  yellow: PaletteColor;
  red: PaletteColor;
}
