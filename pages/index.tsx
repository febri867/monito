import React from 'react';
import dynamic from 'next/dynamic';
import localFont from 'next/font/local';

const ViewNoSSR = dynamic(() => import('@/modules/dashboard'), {
  ssr: false,
});

const Gilroy = localFont({ src: '../assets/fonts/svngilroy/Gilroy-Regular.woff2' });

const Dashboard = (props: any) => {
  return (
    <main className={Gilroy.className}>
      <ViewNoSSR {...props} />
    </main>
  );
};

export default Dashboard;
