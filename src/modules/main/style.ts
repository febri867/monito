import styled from '@emotion/styled';

export const MainStyled = styled.div`
  height: 100vh;
  max-height: 100vh;
  overflow: hidden;
  background: linear-gradient(102.87deg, #fceed5 6.43%, #fceed5 78.33%, #ffe7ba 104.24%);
  .image-bc__home {
    position: absolute;
  }

  .persona-home {
    height: auto;
    right: 0;
    bottom: 0;
  }
  .rect1 {
    height: auto;
    right: 265px;
    bottom: 0;
  }
  .rect2 {
    height: auto;
    right: 250px;
    bottom: 0;
  }
  .rect3 {
    height: auto;
    left: 200px;
    top: 230px;
  }
  .rect5 {
    height: auto;
    right: 750px;
    bottom: 600px;
  }
  .rect6 {
    height: auto;
    right: 770px;
    bottom: 530px;
  }
  .rect7 {
    height: auto;
    right: 775px;
    bottom: 525px;
  }
  .rect8 {
    height: auto;
    left: 0;
    bottom: 0;
  }
  .rect9 {
    height: auto;
    left: 0;
    top: 0;
  }

  .app-bar {
    background-color: transparent;
    border: none;
    box-shadow: none;

    .container-menu {
      justify-content: center;
    }
    .btn-menu {
      font-family: inherit;
    }
  }
`;
