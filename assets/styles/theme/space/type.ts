export interface SpaceOptions {
  padding?: {
    p0: string;
    p50: string;
    p100: string;
    p150: string;
    p200: string;
  };
  margin?: {
    m0: string;
    m50: string;
    m100: string;
    m150: string;
    m200: string;
  };
}

export interface Space {
  padding: {
    p0: string;
    p50: string;
    p100: string;
    p150: string;
    p200: string;
  };
  margin: {
    m0: string;
    m50: string;
    m100: string;
    m150: string;
    m200: string;
  };
}
