import { css } from '@emotion/react';

const value = {
  'text-center': 'center',
};

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    result += `.${valKey}{ text-align: ${val} }`;
  });
  return result;
};

const alignment = css`
  ${generatedStyle()}
`;

export default alignment;
