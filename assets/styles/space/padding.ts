import { css } from '@emotion/react';

import theme from '@/assets/styles/theme';

const value = theme.space.padding;

const positions = ['', '-top', '-bottom', '-right', '-left'];

const generatedStyle = () => {
  let result = '';
  const values = new Map(Object.entries(value));
  values.forEach((val, valKey) => {
    positions.forEach((pos) => {
      result += `.${valKey}${pos}{ padding${pos}: ${val} }`;
    });
  });
  return result;
};

const padding = css`
  ${generatedStyle()}
`;

export default padding;
