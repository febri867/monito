import { PaletteOptions } from './type';

const palette: PaletteOptions = {
  common: {
    white: '#ffffff',
    black: '#000000',
  },

  primary: {
    main: '#002A48',
    light: '#93d6ce',
    lighter: '#caf7f2',
    dark: '#008f7f',
    contrastText: '#ffffff',
    contrast40: '#47C1B4',
  },

  cactus: {
    main: '#21bf73',
    light: '#d2f2e3',
    dark: '#008f7f',
    contrastText: '#ffffff',
    contrast10: '#E6FFFD',
  },

  mist: {
    main: '#bdbdbd',
    light: '#eff0f2',
    dark: '#515353',
    lighter: '#F6F5F4',
    contrastText: '#ffffff',
    contrast40: '#C8C6C0',
    contrast50: '#A6A39D',
    contrast60: '#5A5852',
    contrast70: '#43423C',
    contrast80: '#302F2D',
    contrast100: '#050504',
  },

  ashpalth: {
    main: '#5c6277',
    light: '#868686',
    dark: '#1c1c1e',
    contrastText: '#ffffff',
    contrast10: '#E3E2DF',
    contrast40: '#5B5953',
  },

  yellow: {
    main: '#FF9E1B',
    light: '',
    dark: '',
    contrastText: '#ffffff',
  },

  red: {
    main: '#DD2C00',
    light: '',
    dark: '',
    contrastText: '',
  },
};

export default palette;
