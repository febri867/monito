import { SpaceOptions } from './type';

const space: SpaceOptions = {
  padding: {
    p0: '0rem',
    p50: '0.5rem',
    p100: '1rem',
    p150: '1.5rem',
    p200: '2rem',
  },
  margin: {
    m0: '0rem',
    m50: '0.5rem',
    m100: '1rem',
    m150: '1.5rem',
    m200: '2rem',
  },
};

export default space;
