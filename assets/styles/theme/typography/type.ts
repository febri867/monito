export interface TypographyOptions {
  htmlFontSize?: number;
  fontFamily?: string;
  size: {
    size75: string;
    size80: string;
    size90: string;
    size110: string;
    size115: string;
    size120: string;
    size150: string;
    size200: string;
  };
  weight: {
    weight100: string;
    weight200: string;
    weight300: string;
    weight400: string;
    weight500: string;
    weight600: string;
    weight700: string;
    weight900: string;
  };
  h1?: React.CSSProperties;
  h2?: React.CSSProperties;
  h3?: React.CSSProperties;
  titleBlack?: React.CSSProperties;
  titleMedium?: React.CSSProperties;
  contentBlack?: React.CSSProperties;
  contentMedium?: React.CSSProperties;
  contentBook?: React.CSSProperties;
  smallBlack?: React.CSSProperties;
  smallMedium?: React.CSSProperties;
}

export interface Typography {
  htmlFontSize: true;
  fontFamily: true;
  size: true;
  weight: true;
  h1: true;
  h2: true;
  h3: true;
  titleBlack: true;
  titleMedium: true;
  contentBlack: true;
  contentMedium: true;
  contentBook: true;
  smallBlack: true;
  smallMedium: true;
}
