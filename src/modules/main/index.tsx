import { AppBar, Box, Button, Container, IconButton, Toolbar, Typography } from '@mui/material';
import { MainStyled } from '@/modules/main/style';
import Image from 'next/image';

const Main = () => {
  const pages = ['Products', 'Pricing', 'Blog'];
  const settings = ['Profile', 'Account', 'Dashboard', 'Logout'];
  return (
    <MainStyled>
      <Image
        className={'image-bc__home rect1'}
        src={'/image/Rectangle 1.png'}
        loading="lazy"
        alt={'background'}
        width={'635'}
        height={'635'}
      />
      <Image
        className={'image-bc__home rect2'}
        src={'/image/Rectangle 2.png'}
        loading="lazy"
        alt={'background'}
        width={'635'}
        height={'635'}
      />
      <Image
        className={'image-bc__home rect3'}
        src={'/image/Rectangle 3.png'}
        loading="lazy"
        alt={'background'}
        width={'67'}
        height={'67'}
      />
      <Image
        className={'image-bc__home rect5'}
        src={'/image/Rectangle 5.png'}
        loading="lazy"
        alt={'background'}
        width={'15'}
        height={'15'}
      />
      <Image
        className={'image-bc__home rect6'}
        src={'/image/Rectangle 6.png'}
        loading="lazy"
        alt={'background'}
        width={'28'}
        height={'28'}
      />
      <Image
        className={'image-bc__home rect7'}
        src={'/image/Rectangle 7.png'}
        loading="lazy"
        alt={'background'}
        width={'21'}
        height={'21'}
      />
      <Image
        className={'image-bc__home rect8'}
        src={'/image/Rectangle 8.png'}
        loading="lazy"
        alt={'background'}
        width={'635'}
        height={'635'}
      />
      <Image
        className={'image-bc__home rect9'}
        src={'/image/Rectangle 9.png'}
        loading="lazy"
        alt={'background'}
        width={'635'}
        height={'635'}
      />
      <Image
        className={'image-bc__home persona-home'}
        src={'/image/Persona-home.png'}
        loading="lazy"
        alt={'background'}
        width={'944'}
        height={'693'}
      />
      <AppBar className={'app-bar'} position="static">
        <Container maxWidth="xl">
          <Toolbar disableGutters>
            <Box
              className={'container-menu'}
              sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' } }}
            >
              <Image
                className={'logo'}
                src={'/image/logo.png'}
                loading="lazy"
                alt={'background'}
                width={'115'}
                height={'40'}
              />
              {pages.map((page) => (
                <Button key={page} className={'btn-menu'} sx={{ my: 2, display: 'block' }}>
                  {page}
                </Button>
              ))}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
    </MainStyled>
  );
};

export default Main;
