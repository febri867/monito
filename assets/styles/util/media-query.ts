import facepaint from 'facepaint';

import theme from '@/assets/styles/theme';

const { values } = theme.breakpoints;

const breakpoints = Object.keys(values).map((bpKey) => values[bpKey as keyof typeof values]);

export const mediaQuery = facepaint(
  breakpoints.filter((bp) => bp > 0).map((bp) => `@media (min-width: ${bp}px)`),
);
