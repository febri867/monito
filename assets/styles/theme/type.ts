import { Theme as MuiTheme, ThemeOptions as MuiThemeOptions } from '@mui/material/styles';

import { PaletteOptions, Palette } from '@/assets/styles/theme/palette/type';
import {
  CustomComponentsOptions,
  CustomComponents,
} from '@/assets/styles/theme/custom-components/type';
import { SpaceOptions, Space } from '@/assets/styles/theme/space/type';
import { DimensionOptions, Dimension } from '@/assets/styles/theme/dimension/type';

export interface ThemeOptions extends MuiThemeOptions {
  palette?: PaletteOptions;
  customComponents?: CustomComponentsOptions;
  space?: SpaceOptions;
  dimension?: DimensionOptions;
}

export interface Theme extends MuiTheme {
  palette: Palette;
  customComponents: CustomComponents;
  space: Space;
  dimension: Dimension;
}
