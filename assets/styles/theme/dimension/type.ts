export interface DimensionOptions {
  width?: {
    w1: string;
    w100: string;
    w150: string;
  };
}

export interface Dimension {
  width: {
    w1: string;
    w100: string;
    w150: string;
  };
}
