import { ThemeProvider as EmotionThemeProvider, ThemeProviderProps } from '@emotion/react';

import theme from '../theme';

export type IThemeProvider = Pick<ThemeProviderProps, 'children'>;

const ThemeProvider = ({ children }: IThemeProvider) => {
  return <EmotionThemeProvider theme={theme}>{children}</EmotionThemeProvider>;
};

export default ThemeProvider;
