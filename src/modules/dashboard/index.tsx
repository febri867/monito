import ReactPageScroller from 'react-page-scroller';
import Main from '@/modules/main';
import Pets from '@/modules/pets';
import { useRef } from 'react';

export default function Dashboard() {
  return (
    <>
      <ReactPageScroller>
        <Main />
        <Pets />
      </ReactPageScroller>
    </>
  );
}
