import { css } from '@emotion/react';

import { Theme } from './theme/type';

const Style = (props: Theme) => {
  const {
    typography,
    palette,
    customComponents: { modal },
  } = props;
  return css`
    body {
      margin: 0;
    }

    html {
      font-size: ${typography.htmlFontSize}px;
      color: ${palette.primary.main};
    }

    [data-rsbs-overlay] {
      z-index: ${modal.contentZIndex} !important;
    }
  `;
};

export default Style;
