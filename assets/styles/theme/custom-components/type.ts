export interface CustomComponentsOptions {
  modal?: {
    maxWidth: number[];
    bgZIndex: number;
    contentZIndex: number;
  };
  topRibbon?: {
    height: number[];
    padding: number[][];
    checkWidth: number[];
    zIndex: number;
  };
  stepAuth: {
    maxWidth: number;
    height: number;
  };
}

export interface CustomComponents {
  modal: {
    maxWidth: number[];
    bgZIndex: number;
    contentZIndex: number;
  };
  topRibbon: {
    height: number[];
    padding: number[][];
    checkWidth: number[];
    zIndex: number;
  };
  stepAuth: {
    maxWidth: number;
    height: number;
  };
}
